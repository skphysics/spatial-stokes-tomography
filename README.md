# Spatial Stokes Tomography

Example code used to simulate spatial Stokes tomography measurement on a generally mixed spatial state, such as the beam of a vertical cavity surface-emitting laser diode explored in https://arxiv.org/abs/2202.01932  

The example is set by default to do the spatial state tomography measurement on a pure state (Gaussian beam) but the code has option to switch to a mixed state that was observed experimentally, with the same probability distribution of the modes as that observed in the experiment.

You can also include measurement noise and see its effect on the spatial state reconstruction. 

## Installation
You will need two custom modules included 'plots.py' and 'modes.py'. In case there are some nested dependencies, please pip install those. I hope I did not forget nested dependencies of my own. If there is a module name that you can't find online that is required, please let me know and I will fix it.

## Support
In case of any encountered issues (very likely at this stage), let me know via issue tracker or at my email: m.ploschner@uq.edu.au

## Roadmap
The code is in a pretty raw state. It contains a lot of debugging flags and I made no attempts to optimise, beutify or streamline it. I am happy to make the code much more accessible if there is interest in the community. 

## Contributing
I am happy to implement contributions and suggestions from others and make the code of higher-quality.

## Authors and acknowledgment
Author: Martin Ploschner. 
Acknowledgement: Thanks to all those amazing people on https://stackoverflow.com/ finding solutions to all my questions. 

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 

If you use the code in your research endeavours, please cite the following work https://arxiv.org/abs/2202.01932

## Project status
Active
